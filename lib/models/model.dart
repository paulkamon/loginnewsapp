class NewsApiModel {
  String author, title, imageUrl, content, description;
  DateTime publishedAt;

  NewsApiModel(
      {required this.author,
      required this.title,
      required this.description,
      required this.content,
      required this.imageUrl,
      required this.publishedAt});


  factory NewsApiModel.fromJson(Map<String, dynamic> jsonData) {
    return NewsApiModel(
      author: jsonData ['author'] ?? "",
      title: jsonData['title'] ?? "",
      description: jsonData['description'] ?? "",
      content: jsonData['content'] ?? "",
      imageUrl: jsonData['urlToImage'] ?? "",
      publishedAt: DateTime.parse(jsonData['publishedAt'] ?? "") ,
    );
  }
}
