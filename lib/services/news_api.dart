import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/model.dart';

Future<List<NewsApiModel>> getNews() async {
  Uri uri = Uri.parse(
      "https://newsapi.org/v2/everything?q=tesla&from=2022-03-07&sortBy=publishedAt&apiKey=ad28bbb663e44c24ae3d219b19d9062d");

  final response = await http.get(uri);
  if (response.statusCode == 200 || response.statusCode == 201) {
    Map<String, dynamic> map = json.decode(response.body);
    List _articalsList = map['articles'];
    List<NewsApiModel> newsList = _articalsList
        .map((jsonData) => NewsApiModel.fromJson(jsonData))
        .toList();
    print(_articalsList);
    return newsList;
  } else {
    print("error");
    return [];
  }
}
