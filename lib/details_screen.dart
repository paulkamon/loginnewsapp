import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Widget/customClipper.dart';
import 'models/model.dart';

class DetailNews extends StatelessWidget {
  final NewsApiModel model;

  const DetailNews({required this.model, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final format = DateFormat.yMMMMEEEEd('fr');
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: size.height,
          width: size.width,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: -MediaQuery.of(context).size.height * .15,
                right: -MediaQuery.of(context).size.width * .4,
                child: Container(
                    child: Transform.rotate(
                      angle: -pi / 3.5,
                      child: ClipPath(
                        clipper: ClipPainter(),
                        child: Container(
                          height: MediaQuery.of(context).size.height * .5,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Color(0xffE6E6E6),
                                Color(0xff14279B),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
              ),
              SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      width: size.width / 1.05,
                      alignment: Alignment.center,
                      child: Text(
                        model.title,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 26,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Container(
                      height: size.height / 3,
                      width: size.width ,
                      alignment: Alignment.center,
                      child: model.imageUrl != ""
                          ? Image.network(
                        model.imageUrl,
                        fit: BoxFit.cover,
                      )
                          : Text(
                        "Unable to load image",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Container(
                      width: size.width / 1.05,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "author: "+model.author,
                        textAlign: TextAlign.justify,
                        maxLines: 10,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Container(
                      width: size.width / 1.05,
                      child: Text(
                        format.format(model.publishedAt),
                        maxLines: 2,
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 15,
                        ),
                      ),
                    ),
                    Container(
                      height: size.height,
                      width: size.width / 1.05,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                        model.content,
                        textAlign: TextAlign.justify,
                        maxLines: 10,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ],
                ),
              ),

            ],
          ),

        ),
      ),
    );
  }
}
