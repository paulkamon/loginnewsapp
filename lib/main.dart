import 'package:flutter/material.dart';
import 'login_screen.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    initializeDateFormatting('fr');
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'SanFrancisco',
        primarySwatch: Colors.blue,
        // textTheme:GoogleFonts.latoTextTheme(textTheme).copyWith(
        //   bodyText1: GoogleFonts.montserrat(textStyle: textTheme.bodyText1),
        // ),
      ),
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}